#pragma once

#include "BaseObject.h"

class Player : public BaseObject
{
public:
	Player(sf::Texture& texture, Level& LEV, float X, float Y, float W, float H);
	~Player();
	
	void update(float& time);
	bool getHit();
	void setHit(bool hit);

private:
	void controlKeyboard();
	void Collision(float DX, float DY);

private:
	enum { stay, walk, down, jump, climb } STATE;
	bool m_onLadder, m_shoot, m_hit;
	int player;
};