#include "movePlatform.h"

movePlatform::movePlatform(sf::Texture& texture, float X, float Y, float W, float H)
	: BaseObject(texture, X, Y, W, H)
{
	m_sprite.setTexture(texture);
	m_sprite.setTextureRect(sf::IntRect(0, 0, W, H));
	setName("movePlatform");
	setHealth(100);
	setDX(-0.05);
}

void movePlatform::update(float &time)
{
	setX(getX() + getDX()*time);
	setMoveTimer(getMoveTimer() + time);

	if (getMoveTimer() > 2000) { setDX(getDX() * -1); setMoveTimer(0); }

	m_sprite.setPosition(getX() + getW() / 2, getY() + getH() / 2);
}

movePlatform::~movePlatform()
{
}