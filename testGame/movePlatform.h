#pragma once

#include "BaseObject.h"

class movePlatform : public BaseObject
{
public:
	movePlatform(sf::Texture& texture, float X, float Y, float W, float H);
	~movePlatform();

	void update(float &time);
};