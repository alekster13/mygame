#include "Game.h"

Game::Game()
	: m_isPlaying(true)
{

}

void Game::Start()
{
	

	initializate();
}

void Game::initializate()
{
	m_renderWindow = new sf::RenderWindow(sf::VideoMode(800, 600), "testGame");
	m_view = new sf::View(sf::FloatRect(0, 0, 600, 400));
	m_lvl = new Level;
	m_lvl->LoadFromFile("files1/mymap.tmx");
	m_bg = new sf::Texture;
	m_bg->loadFromFile("files1/images/scifi_BG.png");
	m_background = new sf::Sprite;
	m_background->setTexture(*m_bg);
	m_background->setOrigin(m_bg->getSize().x / 2, m_bg->getSize().y / 2);

	font.loadFromFile("files1/font.ttf");
	text.setFont(font);
	text.setCharacterSize(20);
	text.setFillColor(sf::Color::Red);
	text.setStyle(sf::Text::Bold);

	m_heroTexture = new sf::Texture;
	m_heroTexture->loadFromFile("files1/images/SM.png");
	m_enemyTexture = new sf::Texture;
	m_enemyTexture->loadFromFile("files1/images/enemy.png");
	m_platform_t = new sf::Texture;
	m_platform_t->loadFromFile("files1/images/movePlatform.png");
	m_bullet_t = new sf::Texture;
	m_bullet_t->loadFromFile("files1/images/bullet.png");

	objPlayerMap = new Object;
	*objPlayerMap = m_lvl->GetObject("player");

	player = new Player(*m_heroTexture, *m_lvl, objPlayerMap->rect.left, objPlayerMap->rect.top - 75, 19, 29);

	
	std::vector<Object> allEnemy = m_lvl->GetObjects("enemy");

	for (auto i : allEnemy)
	{
		objMap.push_back(new Enemy(*m_enemyTexture, *m_lvl, i.rect.left, i.rect.top, 16, 16));
	}

	allEnemy = m_lvl->GetObjects("movePlatform");

	for (auto i : allEnemy)
	{
		objMap.push_back(new movePlatform(*m_platform_t, i.rect.left, i.rect.top, 95, 13));
	}

	allEnemy.clear();
}

bool Game::loop()
{
	if (!m_isPlaying) { return false; }

	float time = m_clock.getElapsedTime().asMicroseconds();
	m_clock.restart();
	time /= 800;

	sf::Event event;
	while (m_renderWindow->pollEvent(event))
	{
		if (event.type == sf::Event::Closed) { m_isPlaying = false; m_renderWindow->close(); }
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::LControl)
			{
				if (player->getLife()) {objMap.push_back(new Bullet(*m_bullet_t, *m_lvl, player->getX(), player->getY(), 16, 16, player->getDir()));}
			}
		}
	}

	update(time);

	processingCollision(time);

	render();

	return m_isPlaying;
}

void Game::render()
{
	cameraForPlayer();
	m_background->setPosition(m_view->getCenter());
	m_renderWindow->draw(*m_background);

	m_lvl->Draw(*m_renderWindow);

	for (auto i = objMap.begin(); i != objMap.end(); i++)
	{
		m_renderWindow->draw((*i)->m_sprite);
	}

	m_renderWindow->draw(player->m_sprite);

	textPanelForPlayer();

	m_renderWindow->draw(text);

	m_renderWindow->display();
}

void Game::update(float &time)
{
	player->update(time);

	for (it = objMap.begin(); it != objMap.end();)
	{
		BaseObject *b = *it;
		b->update(time);
		if (b->getLife() == false) { it = objMap.erase(it); delete b; }
		else { it++; }
	}
}

void Game::processingCollision(float &time)
{
	for (it = objMap.begin(); it != objMap.end(); it++)
	{
		BaseObject *p = *it;

		if (p->getRect().intersects(player->getRect()))
		{
			if (p->getName() == "SimpleEnemy")
			{
				if ((player->getDY() > 0) && (!player->getOnGround())) { p->setDX(0); p->setHealth(0); player->setDY(-0.2); }
				else if (!player->getHit()) { player->setHealth(player->getHealth() - 15); player->setHit(true); 
				if (player->getDir()) { player->setX(player->getX() + 10); } else { player->setX(player->getX() - 10); } }
			}

			if (p->getName() == "movePlatform")
			{
				if ((player->getDY() > 0) || (!player->getOnGround()))
				{
					if (player->getY() + player->getH() < p->getY() + p->getH())
					{
						player->setY(p->getY() - player->getH());
						player->setX(player->getX() + p->getDX() * time);
						player->setDY(0); player->setOnGround(true);
					}
				}

				if (player->getDY() < 0)
				{
					player->setY(p->getY() + p->getH());
					player->setDY(0);
				}
			}
		}

		for (it2 = objMap.begin(); it2 != objMap.end(); it2++)
		{
			BaseObject *p2 = *it2;

			if (p->getRect() != p2->getRect())
			{
				if ((p->getRect().intersects(p2->getRect())) && (p->getName() == "SimpleEnemy") && (p2->getName() == "SimpleEnemy"))
				{
					p->setDX(p->getDX() * -1);
					p->m_sprite.scale(-1, 1);
				}

				if ((p->getRect().intersects(p2->getRect())) && (p->getName() == "bullet") && (p2->getName() == "SimpleEnemy"))
				{
					p2->setHealth(p2->getHealth() - 25);
					p->setLife(false);
				}
			}
		}
	}
}

void Game::textPanelForPlayer()
{
	std::ostringstream playerHealthString;
	playerHealthString << player->getHealth();

	if (player->getLife())
	{
		text.setCharacterSize(10);
		text.setPosition(player->getX() - 5, player->getY() - 20);
		text.setString(playerHealthString.str());
	}
	else
	{
		text.setPosition(m_view->getCenter());
		text.setCharacterSize(100);
		text.setString("DEAD");
	}
}

void Game::cameraForPlayer()
{
	size_t viewPlayer_X = player->getX(); size_t viewPlayer_Y = player->getY();
	std::cout << viewPlayer_Y << std::endl;
	if (viewPlayer_X < 300) { viewPlayer_X = 300; } // ������� ����� ���� ������
	if (viewPlayer_Y > 280) { viewPlayer_Y = 280; } // ������ ����
	if (viewPlayer_X > 660) { viewPlayer_X = 660; } // ������ ����
	if (viewPlayer_Y < 200) { viewPlayer_Y = 200; } // ������� ����
	m_view->setCenter(viewPlayer_X, viewPlayer_Y);
	//m_view->setViewport(sf::FloatRect(0, 0, 0.9f, 1));
	m_renderWindow->setView(*m_view);
}

Game::~Game()
{
	if (m_renderWindow) { delete m_renderWindow; }
	if (m_view) { delete m_view; }
	if (m_lvl) { delete m_lvl; }
	if (m_bg) { delete m_bg; }
	if (m_background) { delete m_background; }
	if (player) { delete player; }
	if (m_heroTexture) { delete m_heroTexture; }
	if (objPlayerMap) { delete objPlayerMap; }
	if (m_enemyTexture) { delete m_enemyTexture; }
	if (enemy) { delete enemy; }
	if (m_platform_t) { delete m_platform_t; }
	if (platform) { delete platform; }
	if (m_bullet_t) { delete m_bullet_t; }
	if (bullet) { delete bullet; }
}