#include "Game.h"

int main()
{
 
	Game *game = new Game;
	game->Start();
	while (game->loop());

	delete game;

  return 0;
}
