#pragma once

#include "BaseObject.h"

class Bullet : public BaseObject
{
public:
	Bullet(sf::Texture& texture, Level &lev, float X, float Y, float W, float H, int dir);
	~Bullet();

	void update(float &time);

private:

};