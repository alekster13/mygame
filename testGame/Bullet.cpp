#include "Bullet.h"

Bullet::Bullet(sf::Texture& texture, Level &lev, float X, float Y, float W, float H, int dir)
	:BaseObject(texture, X, Y, W, H)
{
	m_obj = lev.GetObjects("solid");
	setName("bullet");
	setDX(0.3);
	if (dir) { setDX(-0.3); }
	m_sprite.setTexture(texture);
	m_sprite.setTextureRect(sf::IntRect(0, 0, W, H));
}

void Bullet::update(float &time)
{
	setX(getX() + getDX() * time);

	for (int i = 0; i < m_obj.size(); i++)
	{
		if (getRect().intersects(m_obj[i].rect))
		{
			setHealth(0);
			setLife(false);
		}
	}

	m_sprite.setPosition(getX() + getW() / 2, getY() + getH() / 2);
}

Bullet::~Bullet()
{
}