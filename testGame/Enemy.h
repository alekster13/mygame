#pragma once

#include "BaseObject.h"

class Enemy : public BaseObject
{
public:
	Enemy(sf::Texture& texture, Level& lev, float X, float Y, int W, int H);
	~Enemy();

	void update(float& time);

private:
	void CollisionWithMap(float DX, float DY);
	float t;
};