#ifndef GAME_H
#define GAME_H


#include "level.h"
#include "BaseObject.h"
#include "Player.h"
#include "Enemy.h"
#include "movePlatform.h"
#include "Bullet.h"
#include <list>
#include <vector>
#include <sstream>
#include <memory>


class Game
{
public:
	Game();
	~Game();
	void Start();
	bool loop();

private:
	void initializate();
	void update(float &time);
	void render();
	void processingCollision(float &time);
	void cameraForPlayer();
	void textPanelForPlayer();

private:
	bool m_isPlaying;

	sf::RenderWindow* m_renderWindow;
	sf::Clock m_clock;
	sf::View* m_view;
	Level* m_lvl;

	sf::Texture* m_bg, *m_heroTexture, *m_enemyTexture, *m_platform_t, *m_bullet_t;
	sf::Sprite* m_background;


	std::list<BaseObject*> objMap;
	std::list<BaseObject*>::iterator it;
	std::list<BaseObject*>::iterator it2;

	Player *player;
	Object *objPlayerMap;
	Enemy *enemy;
	movePlatform *platform;
	Bullet *bullet;

	sf::Font font;
	sf::Text text;
};

#endif GAME_H