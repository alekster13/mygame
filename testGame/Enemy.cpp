#include "Enemy.h"

Enemy::Enemy(sf::Texture& texture, Level& lev, float X, float Y, int W, int H)
	: BaseObject(texture, X, Y, W, H)
{
	setHealth(75);
	m_obj = lev.GetObjects("solid");
	m_sprite.setTexture(texture);
	m_sprite.setTextureRect(sf::IntRect(0, 0, W, H));
	setName("SimpleEnemy");
	setDX(0.05);
}

void Enemy::CollisionWithMap(float DX, float DY)
{
	for (int i = 0; i < m_obj.size(); i++)
	{
		if (getRect().intersects(m_obj[i].rect))
		{
			if (m_obj[i].name == "solid")
			{
				if (DY > 0) { setY(m_obj[i].rect.top - getH()); setDY(0); }
				if (DY < 0) { setY(m_obj[i].rect.top + m_obj[i].rect.height); setDY(0); }
				if (DX > 0) { setX(m_obj[i].rect.left - getW()); setDX(-0.05); }
				if (DX < 0) { setX(m_obj[i].rect.left + m_obj[i].rect.width); setDX(0.05); }
			}
		}
	}
}

void Enemy::update(float& time)
{
	if (getName() == "SimpleEnemy")
	{
		setDY(getDY() + 0.0005*time);
		setMoveTimer(getMoveTimer() + time);
		if (getMoveTimer() > 4000) { setDX(getDX() * -1); setMoveTimer(0); }

		setX(getX() + getDX()*time);
		CollisionWithMap(getDX(), 0);

		setY(getY() + getDY()*time);
		CollisionWithMap(0, getDY());

		m_sprite.setPosition(getX() + getW() / 2, getY() + getH() / 2);
		if (getHealth() <= 0) { setLife(false); }
	}
}

Enemy::~Enemy()
{
}