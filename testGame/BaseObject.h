#pragma once

#include <SFML\Graphics.hpp>
#include <vector>
#include "level.h"

class BaseObject
{
public:
	BaseObject(sf::Texture& texture, float X, float Y, float W, float H);
	virtual ~BaseObject();

	virtual void update(float& TIME) = 0;
	sf::FloatRect getRect();
	void setCharacteristicsObject(std::string NAME, float SPEED, int HEALTH);
	//void properties(std::string NAME, float SPEED, int HEALTH)  // TODO

public:
	float getX();
	float getY();
	float getW();
	float getH();
	float getDX();
	float getDY();
	float getDir();
	float getSpeed();
	float getMoveTimer();
	bool  getLife();
	bool  getIsMove();
	bool  getOnGround();
	std::string getName();
	int   getHealth();

	void setX(float X);
	void setY(float Y);
	void setW(float W);
	void setH(float H);
	void setDX(float DX);
	void setDY(float DY);
	void setDir(float DIR);
	void setSpeed(float SPEED);
	void setMoveTimer(float TIMER);
	void setLife(bool LIFE);
	void setIsMove(bool MOVE);
	void setOnGround(bool GROUND);
	void setName(std::string NAME);
	void setHealth(int HEALTH);

public:
	std::vector<Object> m_obj;
	sf::Sprite m_sprite;

private:
	float m_x, m_y, m_w, m_h, m_dx, m_dy, m_dir, m_speed, m_moveTimer;
	bool  m_life, m_isMove, m_onGround;
	std::string  m_name;
	int  m_health;
};