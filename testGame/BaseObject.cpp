#include "BaseObject.h"

BaseObject::BaseObject(sf::Texture& texture, float X, float Y, float W, float H)
	: m_x(X), m_y(Y), m_w(W), m_h(H)
	, m_dx(0), m_dy(0), m_dir(0)
	, m_onGround(false), m_isMove(false), m_life(true)
	, m_speed(0)
	, m_health(0)
	, m_moveTimer(0)
{
	m_sprite.setTexture(texture);
	m_sprite.setOrigin(m_w / 2, m_h / 2);
}

sf::FloatRect BaseObject::getRect() { return sf::FloatRect(m_x, m_y, m_w, m_h); }

void BaseObject::setCharacteristicsObject(std::string NAME, float SPEED, int HEALTH)
{
	m_name = NAME;
	m_dx = SPEED;
	m_health = HEALTH;
}

float BaseObject::getX()					   { return m_x; }
float BaseObject::getY()					   { return m_y; }
float BaseObject::getW()					   { return m_w; }
float BaseObject::getH()					   { return m_h; }
float BaseObject::getDX()					   { return m_dx; }
float BaseObject::getDY()					   { return m_dy; }
float BaseObject::getDir()					   { return m_dir; }
float BaseObject::getSpeed()				   { return m_speed; }
float BaseObject::getMoveTimer()			   { return m_moveTimer; }
bool BaseObject::getLife()					   { return m_life; }
bool BaseObject::getIsMove()				   { return m_isMove; }
bool BaseObject::getOnGround()				   { return m_onGround; }
std::string BaseObject::getName()			   { return m_name; }
int BaseObject::getHealth()					   { return m_health; }

void BaseObject::setX(float X)				   { m_x = X; }
void BaseObject::setY(float Y)				   { m_y = Y; }
void BaseObject::setW(float W)				   { m_w = W; }
void BaseObject::setH(float H)				   { m_h = H; }
void BaseObject::setDX(float DX)			   { m_dx = DX; }
void BaseObject::setDY(float DY)			   { m_dy = DY; }
void BaseObject::setDir(float DIR)			   { m_dir = DIR; }
void BaseObject::setSpeed(float SPEED)		   { m_speed = SPEED; }
void BaseObject::setMoveTimer(float TIMER)	   { m_moveTimer = TIMER; }
void BaseObject::setLife(bool LIFE)			   { m_life = LIFE; }
void BaseObject::setIsMove(bool MOVE)		   { m_isMove = MOVE; }
void BaseObject::setOnGround(bool GROUND)	   { m_onGround = GROUND; }
void BaseObject::setName(std::string NAME)	   { m_name = NAME; }
void BaseObject::setHealth(int HEALTH)		   { m_health = HEALTH; }

BaseObject::~BaseObject() {}