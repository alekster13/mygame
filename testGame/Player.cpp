#include "Player.h"

Player::Player(sf::Texture& texture, Level& LEV, float X, float Y, float W, float H)
	: BaseObject(texture, X, Y, W, H)
	, STATE(stay)
	, m_hit(false), m_shoot(false), m_onLadder(false)
{
	setHealth(100);
	m_obj = LEV.GetAllObjects();
	m_sprite.setTexture(texture);
	m_sprite.setTextureRect(sf::IntRect(0, 0, W, H));
}

void Player::controlKeyboard()
{
	//if (!getLife()) { setDX(0); setDY(0); return; }

	if (sf::Keyboard::isKeyPressed)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) { setDir(1); setSpeed(-0.1); setDX(getSpeed()); STATE = walk; m_sprite.setTextureRect(sf::IntRect(0 + getW(), 0, -getW(), getH())); }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) { setDir(0); setSpeed(0.1); setDX(getSpeed()); STATE = walk; m_sprite.setTextureRect(sf::IntRect(0, 0, getW(), getH())); }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && (getOnGround())) 
		{ setDY(-0.27); STATE = jump; setOnGround(false); }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) { setSpeed(0); setDX(getSpeed()); STATE = down; }
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && (m_onLadder)) { setDY(-0.05); STATE == climb; }
	}
}

void Player::update(float& time)
{
	controlKeyboard();
	
	if (STATE == walk) { setDX(getSpeed());  }
	if (STATE != walk) { setSpeed(0); setDX(getSpeed()); }
	if (STATE == climb) { if (!m_onLadder) { STATE = stay; } }
	if (STATE != climb) { setDY(getDY() + 0.0005*time); m_onLadder = false; }

	setX(getX() + getDX()*time);
	Collision(getDX(), 0);

	setY(getY() + getDY()*time);
	Collision(0, getDY());

	m_sprite.setPosition(getX() + getW() / 2, getY() + getH() / 2);

	if (m_hit) 
	{
		setMoveTimer(getMoveTimer() + time); if (getMoveTimer() > 2000) { m_hit = false;  setMoveTimer(0); }
	}

	if (getHealth() <= 0) { setLife(false); }
}

void Player::Collision(float DX, float DY)
{
	if (!getLife()) { return; }

	for (int i = 0; i < m_obj.size(); i++)
	{
		if (getRect().intersects(m_obj[i].rect))
		{
			if (m_obj[i].name == "solid")
			{
				if (DY > 0) { setY(m_obj[i].rect.top - getH());  setDY(0);  STATE = stay; setOnGround(true); }
				if (DY < 0)	{ setY(m_obj[i].rect.top + m_obj[i].rect.height); setDY(0); }
				if (DX > 0)	{ setX(m_obj[i].rect.left - getW()); }
				if (DX < 0)	{ setX(m_obj[i].rect.left + m_obj[i].rect.width); }
			}

			if (m_obj[i].name == "ladder")	{ m_onLadder = true; }

			if (m_obj[i].name == "thorns") { setHealth(0); }
		}
	}
}

bool Player::getHit()
{
	return m_hit;
}

void Player::setHit(bool hit)
{
	m_hit = hit;
}

Player::~Player() {}